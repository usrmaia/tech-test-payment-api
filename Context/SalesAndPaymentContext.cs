using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class SalesAndPaymentContext : DbContext
    {
        public SalesAndPaymentContext(DbContextOptions<SalesAndPaymentContext> options) : base(options)
        {
            
        }

        public DbSet<Sale> Sale { get; set; }
        public DbSet<Salesman> Salesmen { get; set; }
        public DbSet<Product> Products { get; set; }

        /*
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sale>()
                .Property(s => s.Status)
                .HasDefaultValue(SaleStatus.AguardandoPagamento);
        }
        */
    }
}