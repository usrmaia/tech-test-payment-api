using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("sale")]
    public class SaleController : ControllerBase
    {
        private readonly SalesAndPaymentContext _context;
        
        public SaleController(SalesAndPaymentContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Create(Sale sale)
        {
            if (!ModelState.IsValid) return BadRequest();

            _context.Sale.Add(new Sale{SalesmanId=sale.SalesmanId, Date=sale.Date, Status=SaleStatus.AguardandoPagamento});

            _context.SaveChanges();
            
            foreach (var item in sale.Products)
            {
                _context.Products.Add(new Product{Name=item.Name, SaleId=_context.Sale.OrderBy(x=>x.Id).Last().Id});
            }

            _context.SaveChanges();

            return Ok(sale);
        }

        [HttpGet]
        public IActionResult GetSales()
        {
            return Ok(_context.Sale);
        }

        [HttpGet("{id}")]
        public IActionResult GetSales(int id)
        {
            var sale = _context.Sale.Find(id);

            if (sale == null) return NotFound();
            return Ok(sale);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateSaleStatus(int id, SaleStatus status)
        {
            var sale = _context.Sale.Find(id);

            if (sale == null) return BadRequest();

            SaleStatus newStatus = GetNewStatus(sale, status);
            
            if (newStatus == status) return BadRequest($"{sale.Status} cannot go to {status}");

            sale.Status = newStatus;

            _context.Sale.Update(sale);
            _context.SaveChanges();

            return Ok(sale);
        }

        private SaleStatus GetNewStatus(Sale sale, SaleStatus status)
        {
            /*
            De: Aguardando pagamento Para: Pagamento Aprovado
            De: Aguardando pagamento Para: Cancelada
            De: Pagamento Aprovado Para: Enviado para Transportadora
            De: Pagamento Aprovado Para: Cancelada
            De: Enviado para Transportador. Para: Entregue
            Case contraio: null
            */  
            switch (sale.Status)
            {
                case SaleStatus.AguardandoPagamento:
                    switch (status)
                    {
                        case SaleStatus.PagamentoAprovado:
                            return SaleStatus.PagamentoAprovado;
                        case SaleStatus.Cancelada:
                            return SaleStatus.Cancelada;
                        default: 
                        break;
                    }
                break;
                case SaleStatus.PagamentoAprovado:
                    switch (status)
                    {
                        case SaleStatus.EnviadoParaTransportadora:
                            return SaleStatus.EnviadoParaTransportadora;
                        case SaleStatus.Cancelada:
                            return SaleStatus.Cancelada;
                        default:
                        break;
                    }
                break;
                case SaleStatus.EnviadoParaTransportadora:
                    switch (status)
                    {
                        case SaleStatus.Entregue:
                            return SaleStatus.Entregue;
                        default:
                        break;
                    }
                break;
                default: 
                break;
            }

            return status;
        }
    }
}