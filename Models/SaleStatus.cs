namespace tech_test_payment_api.Models
{
    public enum SaleStatus
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada, 
    }
}