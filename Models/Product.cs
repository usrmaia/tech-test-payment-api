using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [ForeignKey("Sale")]
        public int? SaleId { get; set; }
        public virtual Sale Sale { get; set; }
    }
}