# API Instructions
Imagem da API:
![Imagem da API](./img/Captura%20de%20tela_20230111_093622.png)
## Observação
O número do *status* corresponde à:
```
0: AguardandoPagamento,
1: PagamentoAprovado,
2: EnviadoParaTransportadora,
3: Entregue,
4: Cancelada
```
## POST
Pode ser simplificado, enviando apenas o essencial:
```
{
  "salesmanId": 4,
  "date": "2023-01-10T21:32:16.253Z",
  "products": [
    {
      "id": 12,
      "name": "Blue"
    },
    {
      "id": 13,
      "name": "Onda"
    }
  ]
}
```
Note que *salesmanId* e *products* devem possuir ids válidas.
## GET
Retornar todas as sales ou apenas uma (caso que informa id).
## PUT
A mudança de status pode ocorrer nessas situações:
```
De: Aguardando pagamento Para: Pagamento Aprovado
De: Aguardando pagamento Para: Cancelada
De: Pagamento Aprovado Para: Enviado para Transportadora
De: Pagamento Aprovado Para: Cancelada
De: Enviado para Transportador. Para: Entregue
```